import express from "express"
import loginController from "../controllers/login.controller.mjs";

const routerLogin = express.Router()

routerLogin.post("/login", loginController.getConnection);

routerLogin.post("/login/admin", loginController.getConnectionAdmin);

export {routerLogin}
