import express from "express"
import {createUpload} from "../middlewares/upload-file.mjs";
import parcelController from "../controllers/parcel.controller.mjs";
import tourController from "../controllers/tour.controller.mjs";
import {create} from "../controllers/deliverer.controller.mjs";

const routerParcel = express.Router()
const upload = createUpload("delivery-proofs")

routerParcel.patch("/parcel/:uuid/delivery", upload.single('proof'), parcelController.deliveryParcel);

routerParcel.get("/parcel/:uuid", parcelController.getParcelByUuid);

routerParcel.post("/parcel", parcelController.create)

export {routerParcel}
