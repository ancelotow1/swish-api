import express from "express"
import tourController from "../controllers/tour.controller.mjs";

const routerTour = express.Router()

routerTour.get("/tours/mine", tourController.getCurrentTourByDeliveryPerson);

routerTour.get('/tours', tourController.getAllTours)

routerTour.get('/tours/:id', tourController.getById)

routerTour.put("/tours/:id", tourController.removeParcelsFromTour)

routerTour.put("/tours/:id/deliverer", tourController.associateTourToDeliverer);

routerTour.patch("/tours/:id/state", tourController.updateState);

export {routerTour}
