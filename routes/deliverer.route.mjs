import {getAll, create, updateByUuid, remove, updateCurrentPosition} from "../controllers/deliverer.controller.mjs";
import express from "express";
import {createUpload} from "../middlewares/upload-file.mjs";
const upload = createUpload("deliverers-upload")

const router = express.Router();

router.get('/deliverer', getAll);

router.post('/deliverer', upload.single('proof'), create);

router.delete('/deliverer/:uuid', remove);

router.put('/deliverer/:uuid', updateByUuid);

router.patch('/deliverer/:uuid/current-position', updateCurrentPosition);

export default router
