import downloadController from "../controllers/download.controller.mjs"
import express from "express";

const router = express.Router();

router.get('/download/parcel/:uuid', downloadController.downloadProofParcel);

router.get('/download/deliverer/:uuid', downloadController.downloadDelivererProfil);

export default router
