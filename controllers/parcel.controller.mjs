import parcelsService from "../services/parcels.service.mjs";
import tourService from "../services/tour.service.mjs";
import tourController from "./tour.controller.mjs";

const DeliveryStatus = {
    waiting: 1,
    preparation: 2,
    in_progress: 3,
    delivered: 4
}


const deliveryParcel = async (req, res) => {
    /*
        #swagger.tags = ['Parcel']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Passe un colis à l\'état livré avec preuve de livraison.'
        #swagger.consumes = ['multipart/form-data']
        #swagger.parameters['date'] = {
               in: 'query',
               description: 'La date de livraison',
               type: 'date',
               required: 'true',
        }
        #swagger.parameters['proof'] = {
               in: 'body',
               description: 'La preuve de livraison',
               type: 'file',
               required: 'true',
        }
        #swagger.responses[204]
    */

    try {
        if(req.query.date === null || req.query.date === undefined) {
            res.status(400).send("The delivery date is mandatory")
        } else if(req.file === null || req.file === undefined) {
            res.status(400).send("The file is required")
        }
        await parcelsService.deliveryParcelByUuid(req.params.uuid, req.query.date, req.file.path);
        res.status(204).send()
    } catch (e) {
        res.status(500).send(e)
    }
}

const getParcelByUuid = async (req, res) => {
    /*
        #swagger.tags = ['Parcel']
        #swagger.description = 'Récupère le détail d\'un colis
    */

    try {
        let parcel = await parcelsService.getParcelByUuid(req.params.uuid);
        if(parcel === null || parcel === undefined) {
            res.status(400).send({
                message: `Not found parcel with uuid ${req.params.uuid}`
            })
        }
        parcel.tour = await tourService.getTourById(parcel.idTour)

        if(parcel.isDelivered) {
            parcel.status = DeliveryStatus.delivered
            res.status(200).send(parcel)
            return
        }

        if(parcel.tour == null) {
            parcel.status = DeliveryStatus.waiting
            res.status(200).send(parcel)
            return
        }

        if(parcel.tour.date === Date.now()) {
            parcel.status = DeliveryStatus.in_progress
            res.status(200).send(parcel)
            return
        }

        parcel.status = DeliveryStatus.preparation
        res.status(200).send(parcel)
    } catch (e) {
        res.status(500).send(e)
    }
}

const downloadProofParcel = async (req, res) => {
    /*
        #swagger.tags = ['Parcel']
        #swagger.description = 'Télécharge la preuve de livraison d\'un colis.'
    */

    try {
        let parcel = await parcelsService.getParcelByUuid(req.params.uuid);
        if(parcel.urlProofDelivered === null || parcel.urlProofDelivered === undefined) {
            res.status(404).send(`Not found parcel delivered with uuid ${req.params.uuid}`)
        } else {
            res.status(200).download(parcel.urlProofDelivered);
        }
    } catch (e) {
        res.status(500).send(e)
    }
}

const create = async (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty!"
        })
        return
    }

    let parcel = Object.assign(new Parcel(), req.body);

    await parcelsService.create(parcel, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Parcel."
            })
        } else {
            res.status(201).send(data)
        }
    })

    // create tour if it can be created
    setTimeout(function() {
        let isCanBeCreated = parcelsService.isTourCanBeCreated()

        if (isCanBeCreated) {
            tourController.create()
        }
    }, 2000);

}

export default {deliveryParcel, getParcelByUuid, downloadProofParcel, create}
