import parcelsService from "../services/parcels.service.mjs";
import DeliveryPersonServices from "../services/delivery-person.service.mjs";

const downloadProofParcel = async (req, res) => {
    /*
        #swagger.tags = ['Download']
        #swagger.description = 'Télécharge la preuve de livraison d\'un colis.'
    */

    try {
        let parcel = await parcelsService.getParcelByUuid(req.params.uuid);
        if(parcel.urlProofDelivered === null || parcel.urlProofDelivered === undefined) {
            res.status(404).send(`Not found parcel delivered with uuid ${req.params.uuid}`)
        } else {
            res.status(200).download(parcel.urlProofDelivered);
        }
    } catch (e) {
        res.status(500).send(e)
    }
}

const downloadDelivererProfil = (req, res) => {
    /*
        #swagger.tags = ['Download']
        #swagger.description = 'Télécharge la photo de profil d\'un livreur.'
    */

    try {
        DeliveryPersonServices.getByUuid(req.params.uuid, (err, delivery) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Deliverer with uuid ${req.params.uuid}`
                    })
                } else {
                    res.status(500).send({
                        message: `Error retrieving Deliverer with uuid ${req.params.uuid}`
                    })
                }
            } else {
                res.status(200).download(delivery.urlPhoto)
            }
        })
    } catch (e) {
        res.status(500).send(e)
    }
}

export default {downloadDelivererProfil, downloadProofParcel}
