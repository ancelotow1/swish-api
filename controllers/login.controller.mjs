import deliveryPersonService from "../services/delivery-person.service.mjs"
import jwt from "jsonwebtoken";
import {Connection} from "../models/connection.mjs";
import {BadRequestError} from "../models/errors/bad-request-error.mjs";

const getConnection = async (req, res) => {
    /*
        #swagger.tags = ['Authentification']
        #swagger.description = 'Connexion à un compte livreur.'
        #swagger.produces = ['application/json']
        #swagger.consumes = ['application/json']
        #swagger.parameters['logins'] = {
            in: 'body',
            description: 'Les informations de connection',
            schema: {$ref: '#/definitions/ConnectionInfo'}
        }
        #swagger.responses[200] = {
            schema: { $ref: "#/definitions/ConnectionSession" },
            description: 'Les informations concernant la session de connection'
        }
    */


    if(!req.body) {
        res.status(400).send("Yous must send logins")
    }
    let deliveryPerson = await deliveryPersonService.getByLogins(req.body.login, req.body.password);
    if(!deliveryPerson) {
        res.status(400).send("The username and/or password are incorrect")
        return
    }
    let connectionInfo = new Connection();
    connectionInfo.deliveryPerson = deliveryPerson;
    connectionInfo.token = generateTokenDeliverer(req.body.login, deliveryPerson.uuid)
    res.status(200).send(connectionInfo)
}

const getConnectionAdmin = async (req, res) => {
    /*
       #swagger.tags = ['Authentification']
       #swagger.description = 'Connexion à un compte administrateur.'
       #swagger.produces = ['application/json']
   */

    res.status(200).send({token: generateTokenAdmin()})
}

const generateTokenAdmin = (login, uuid) => {
    try{
        return jwt.sign({login: null, uuid: null, isAdmin: true}, process.env.TOKEN, { expiresIn: "3d" })
    } catch(e) {
        throw e
    }
}

const generateTokenDeliverer = (login, uuid) => {
    try{
        return jwt.sign({login: login, uuid: uuid, isAdmin: false}, process.env.TOKEN, { expiresIn: "2h" })
    } catch(e) {
        throw e
    }
}

export default {getConnection, getConnectionAdmin}
