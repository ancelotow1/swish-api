import DeliveryPersonServices from "../services/delivery-person.service.mjs";
import {DeliveryPerson} from "../models/delivery-person.mjs";

const getAll = (_, res) => {
    DeliveryPersonServices.getAll((err, data) => {
        if (err) {
            res.status(500).send({
                message: `Error retrieving Deliverers`
            })
            return
        } else {
            res.status(200).send(data)
            return
        }
    })
}

const create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty!"
        })
        return
    }
    console.log(req.body.json)
    const data = JSON.parse(req.body.json)

    console.log("-----------------------------------------------------------")
    console.log(req.file)
    console.log("HERRER 2lansocnsd " + data.name)
    let deliverer = Object.assign(new DeliveryPerson(), data);

    DeliveryPersonServices.create(deliverer, req.file.path, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Deliverer."
            })
        } else {
            res.status(201).send(data)
        }
    })
}

const updateByUuid = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content cannot be empty!"
        })
        return
    }
    let deliverer = Object.assign(new DeliveryPerson(), req.body);

    DeliveryPersonServices.update(req.params.uuid, deliverer, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Deliverer with uuid ${req.params.uuid}`
                })
            } else {
                res.status(500).send({
                    message: `Error retrieving Deliverer with uuid ${req.params.uuid}`
                })
            }
        } else {
            res.status(200).send(data)
        }
    })
}

const remove = (req, res) => {
    const uuid = req.params.uuid
    if (!uuid) {
        res.status(400).send({
            message: "Content cannot be empty!"
        })
        return
    }
    DeliveryPersonServices.remove(uuid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Deliverer with uuid ${req.params.uuid}`
                })
            } else {
                res.status(500).send({
                    message: err.message || "Some error occurred while removing the Deliverer."
                })
            }
            return
        } else {
            res.status(200).send({
                message: "Deliverer deleted successfully!"
            })
        }
    })
}

const updateCurrentPosition = (req, res) => {
    const uuid = req.data.uuid
    if (!uuid) {
        res.status(403).send({
            message: "You are not connected!"
        })
        return
    }
    if(!req.body) {
        res.status(400).send({
            message: "You must send position"
        })
        return
    }
    DeliveryPersonServices.updateCurrentPosition(uuid, req.body, (err) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Deliverer with uuid ${req.params.uuid}`
                })
            } else {
                res.status(500).send({
                    message: err.message || "Some error occurred while updating position the Deliverer."
                })
            }
        } else {
            res.status(204).send()
        }
    })
}


export {getAll, create,  updateByUuid, remove, updateCurrentPosition}
