import tourService from "../services/tour.service.mjs";
import parcelsService from "../services/parcels.service.mjs";
import deliveryPersonService from "../services/delivery-person.service.mjs";
import {BadRequestError} from "../models/errors/bad-request-error.mjs";

const getCurrentTourByDeliveryPerson = async (req, res) => {
    /*
        #swagger.tags = ['Tour']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Récupération de la tournée de livraison du livreur connecté.'
        #swagger.produces = ['application/json']
        #swagger.consumes = ['application/json']
        #swagger.responses[200] = {
            schema: { $ref: "#/definitions/Tour" },
            description: 'La tournée de livraison avec la liste des livraisons à effectuées'
        }
    */

    try {
        let tour = await tourService.getCurrentTourByDelivery(req.data.uuid);
        if(tour === null) {
            res.status(200).send(null)
            return
        }
        tour.parcels = await parcelsService.getParcelsByTour(tour.id);
        tour.deliverer = await deliveryPersonService.getById(tour.idDeliveryPerson)
        res.status(200).send(tour)
    } catch (e) {
        res.status(500).send(e.message)
    }
}


const getAllTours = async (req, res) => {
    /*
        #swagger.tags = ['Tour']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Récupération de toutes les tournées.'
        #swagger.produces = ['application/json']
    */

    try {
        let tours = await tourService.getAllTours();
        if(tours === null) {
            res.status(200).send(null)
            return
        }

        for (const tour of tours) {
            tour.parcels = await parcelsService.getParcelsByTour(tour.id);
            tour.deliverer = await deliveryPersonService.getById(tour.idDeliveryPerson)
        }
        res.status(200).send(tours)
    } catch (e) {
        res.status(500).send(e.message)
    }
}

const getById = async (req, res) => {
    /*
        #swagger.tags = ['Tour']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Récupération une tournée.'
        #swagger.produces = ['application/json']
    */

    try {
        let tour = await tourService.getTourById(req.params.id);
        if(!tour) {
            res.status(400).send(`Not found tour with id ${req.params.id}`)
            return
        }
        tour.parcels = await parcelsService.getParcelsByTour(tour.id);
        tour.deliverer = await deliveryPersonService.getById(tour.idDeliveryPerson)
        res.status(200).send(tour)
    } catch (e) {
        res.status(500).send(e.message)
    }
}

const create = async (req, res) => {
    /*
        #swagger.tags = ['Tour']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Création d'une tournée.'
        #swagger.produces = ['application/json']
    */

    try {
        await tourService.create((err, data) => {
            parcelsService.associateParcelsToTour(data)
        });
        res.status(204).send(null)
    } catch (e) {
        if (e instanceof BadRequestError) {
            res.status(400).send(e.message)
        } else {
            res.status(500).send(e.message)
        }
    }
}

const updateState = async (req, res) => {
    /*
        #swagger.tags = ['Tour']
        #swagger.security = [{ "Bearer": [] }]
        #swagger.description = 'Mets à jour le status de la tournée.'
        #swagger.produces = ['application/json']
    */

    try {
        if(!req.query.state) {
            res.status(400).send("State is required")
            return
        }

        tourService.updateTourState(req.params.id, req.query.state, (err) => {
            if(err) {
                res.status(500).send(err.message)
            } else {
                res.status(204).send(null)
            }
        });
    } catch (e) {
        res.status(500).send(e.message)
    }
}

const removeParcelsFromTour = async (req, res)  => {
    try {
        if(!req.query.state) {
            res.status(400).send("Id Tour is required")
            return
        }

        tourService.removeParcelsFromTour(req.params.id, (err) => {
            if(err) {
                res.status(500).send(err.message)
            } else {
                res.status(204).send(null)
            }
        });
    } catch (e) {
        res.status(500).send(e.message)
    }
}

const associateTourToDeliverer = async (req, res) => {
    try {
        if(!req.query.idDeliverer) {
            res.status(400).send("Id Deliverer is required")
            return
        }


        tourService.associateTourToDeliverer(req.params.id, req.query.idDeliverer, (err) => {
            if(err) {
                res.status(500).send(err.message)
            } else {
                res.status(204).send(null)
            }
        });
    } catch (e) {
        res.status(500).send(e.message)
    }
}

export default {getCurrentTourByDeliveryPerson, getAllTours, create, associateTourToDeliverer, updateState, getById, removeParcelsFromTour}
