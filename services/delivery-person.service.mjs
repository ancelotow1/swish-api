import {pool} from "./postgres.service.mjs";
import {DeliveryPerson} from "../models/delivery-person.mjs";
import {SqlError} from "../models/errors/sql-error.mjs";


const getByLogins = (login, password) => {
    const request = {
        text: `select id,
                      uuid,
                      name,
                      firstname,
                      email,
                      birthday,
                      login,
                      "urlPhoto",
                      "currentLatitude",
                      "currentLongitude"
               from delivery_person
               where login = $1
                 and password = sha256($2)`,
        values: [login, password],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, result) => {
            if (error) {
                reject(new SqlError("Une erreur SQL est survenue lors de la recherche du compte livreur", error));
                return;
            }

            let deliveryPerson = null;
            if (result.rows != null && result.rows.length === 1) {
                deliveryPerson = Object.assign(new DeliveryPerson(), result.rows[0]);
            }
            resolve(deliveryPerson)
        });
    })
}
const getAll = (result) => {
    const request = {

        text: `select id,
                      uuid,
                      name,
                      firstname,
                      email,
                      birthday,
                      login,
                      "urlPhoto",
                      "currentLatitude",
                      "currentLongitude"
               from delivery_person`
    }

    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }

        console.log("found deliverer: ", res)
        result(null, res)
    })
}

const getByUuid = (uuid, result) => {
    const request = {
        text: `select id,
                      uuid,
                      name,
                      firstname,
                      email,
                      birthday,
                      login,
                      "urlPhoto",
                      "currentLatitude",
                      "currentLongitude"
               from delivery_person
               where uuid = $1`,
        values: [uuid],
    }
    pool.query(request, (error, res) => {
        if (error) {
            result(new SqlError("Une erreur SQL est survenue lors de la recherche du compte livreur", error), null);
            return;
        }

        let deliveryPerson = null;
        if (res.rows != null && res.rows.length === 1) {
            deliveryPerson = Object.assign(new DeliveryPerson(), res.rows[0]);
            delete deliveryPerson.password
        }
        result(null, deliveryPerson)
    });
}

const getById = (id) => {
    const request = {
        text: `select id,
                      uuid,
                      name,
                      firstname,
                      email,
                      birthday,
                      login,
                      "urlPhoto",
                      "currentLatitude",
                      "currentLongitude"
               from delivery_person
               where id = $1`,
        values: [id],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, result) => {
            pool.query(request, (error, res) => {
                if (error) {
                    reject(new SqlError("Une erreur SQL est survenue lors de la recherche du compte livreur", error));
                    return;
                }

                let deliveryPerson = null;
                if (res.rows != null && res.rows.length === 1) {
                    deliveryPerson = Object.assign(new DeliveryPerson(), res.rows[0]);
                }
                resolve(deliveryPerson)
            });
        });
    })
}

const updateCurrentPosition = (uuid, position, result) => {
    const request = {
        text: `update delivery_person
               set "currentLatitude"  = $1,
                   "currentLongitude" = $2
               where uuid = $3`,
        values: [position.latitude, position.longitude, uuid],
    }
    pool.query(request, (error, _) => {
        if (error) {
            result(new SqlError("Une erreur SQL est survenue lors de la recherche du compte livreur", error), null);
            return;
        }

        result(null)
    });
}

const create = (newDeliverer, urlPhoto, result) => {
    console.log(" HERRRREEEE " + newDeliverer.name)
    const request = {
        text: `INSERT INTO delivery_person (name, firstname, email, birthday, "urlPhoto", login, password)
               VALUES ($1, $2, $3, $4, $5, $6, sha256($7))`,
        values: [newDeliverer.name, newDeliverer.firstname, newDeliverer.email, newDeliverer.birthday, urlPhoto, newDeliverer.login, newDeliverer.password],
    }
    // "INSERT INTO delivery_person SET ?", newDeliverer
    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        console.log("created : ", {uuid: res.insertId, ...newDeliverer})
        result(null, {uuid: res.insertId, ...newDeliverer})
    })
}

const verifyUndefined = (variable, columnName, arrayWithQueryAndCounter) => {
    if (variable) {
        if (arrayWithQueryAndCounter[1] > 0) {
            arrayWithQueryAndCounter[0] += ", "
        }
        if (columnName === "password") {
            arrayWithQueryAndCounter[0] += `${columnName}= sha256('${variable}')`
            arrayWithQueryAndCounter[1] += 1;
        } else {
            arrayWithQueryAndCounter[0] += `${columnName}='${variable}'`
            arrayWithQueryAndCounter[1] += 1;
        }
    }
}
const update = (uuid, deliverer, result) => {
    const arrayWithQueryAndCounter = ["UPDATE delivery_person SET ", 0];
    const columnName = ["name", "firstname", "email", "birthday", "\"urlPhoto\"", "login", "password"]

    const variable = [deliverer.name, deliverer.firstname, deliverer.email, deliverer.birthday, deliverer.urlPhoto, deliverer.login, deliverer.password]

    // verify if an variable is undefined to not put in the query
    for (let i = 0; i < columnName.length; i++) {
        verifyUndefined(variable[i], columnName[i], arrayWithQueryAndCounter);
    }
    arrayWithQueryAndCounter[0] += ` WHERE uuid='${uuid}';`

    console.log(arrayWithQueryAndCounter[0])
    const request = {
        text: arrayWithQueryAndCounter[0]
    }
    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }

        // not found User with this id
        if (res.rowsAffected === 0) {
            result({kind: "not_found"}, null)
            return
        }

        result(null, {
            uuid: uuid, name: deliverer.name, firstname: deliverer.firstname,
            email: deliverer.email, birthday: deliverer.birthday, urlPhoto: deliverer.urlPhoto, login: deliverer.login
        })
    })
}

const remove = (uuid, result) => {
    console.log(uuid)
    const request = {
        text: `DELETE
               FROM delivery_person
               WHERE uuid = $1`,
        values: [uuid]
    }
    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        if (res.rowsAffected === 0) {
            result({kind: "not_found"}, null)
            return
        }
        result(null, res)
    })
}
export default {getByLogins, getAll, create, update, remove, getByUuid, updateCurrentPosition, getById}
