import {pool} from "./postgres.service.mjs";
import {SqlError} from "../models/errors/sql-error.mjs";
import {Parcel} from "../models/parcel.mjs";
import {TourDelivery} from "../models/tour-delivery.mjs";

const getParcelsByTour = (idTour) => {
    const request = {
        text: `select *
               from parcel
               where "idTour" = $1`,
        values: [idTour],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, result) => {
            if (error) {
                reject(new SqlError("Une erreur SQL est survenue lors de la recherche de la tournée du livreur", error));
                return;
            }

            let parcels = [];
            if (result.rows != null) {
                result.rows.forEach(e => parcels.push(Object.assign(new Parcel(), e)));
            }
            resolve(parcels)
        });
    })
}

const getParcelByUuid = (uuid) => {
    const request = {
        text: `select *
               from parcel
               where "uuid" = $1`,
        values: [uuid],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, result) => {
            if (error) {
                reject(new SqlError("Une erreur SQL est survenue lors de la recherche de la tournée du livreur", error));
                return;
            }

            let parcel;
            if (result.rows != null) {
                parcel = Object.assign(new Parcel(), result.rows[0]);
            }
            resolve(parcel)
        });
    })
}

const deliveryParcelByUuid = (uuid, dateDelivery, pathProof) => {
    const request = {
        text: `update parcel
               set "dateDelivered"      = $1,
                   "isDelivered"        = true,
                   "urlProofDelivered" = $2
               where uuid = $3
                 and "isDelivered" = false`,
        values: [dateDelivery, pathProof, uuid],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, _) => {
            if (error) {
                reject(new SqlError("Une erreur SQL est survenue lors de la mise à jours des informations de livraisons", error));
                return;
            }
            resolve()
        });
    })
}

const isTourCanBeCreated = () => {
    const request = {
        text: `SELECT COUNT(id) as total
               FROM parcel
               where parcel."idTour" IS NULL`,
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (err, res) => {
            if (err) {
                reject(new SqlError("Une erreur SQL est survenue lors de la récupération des colis", err));
                return;
            }

            let isEnoughParcels = false
            if (res.rows != null && res.rows.length === 1) {
                if (res.rows[0].total >= 6) {
                    isEnoughParcels = true
                }
            }
            resolve(isEnoughParcels)
        })
    })
}

const create = (newParcel, result) => {
    const request = {
        text: `INSERT INTO parcel ("addressStreet", town, "zipCode", country, "isDelivered", "urlProofDelivered",
                                   lastname, firstname, civility, email, phone)
               VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING uuid`,
        values: [newParcel.addressStreet, newParcel.town, newParcel.zipCode, newParcel.country, newParcel.isDelivered,
            newParcel.urlProofDelivered, newParcel.lastname, newParcel.firstname, newParcel.civility, newParcel.email,
            newParcel.phone],
    }

    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        console.log("created : ", {uuid: res.rows[0].uuid, ...newParcel})
        result(null, {uuid: res.rows[0].uuid, ...newParcel})
    })
}


const associateParcelsToTour = (idTour) => {

    const request = {
        text: `UPDATE parcel SET "idTour" = $1 WHERE id IN (
    SELECT id FROM parcel WHERE "idTour" IS NULL LIMIT 6
            )`,
        values: [idTour]
    }

    pool.query(request, (err, _) => {
        if (err) {
            console.log("error: ", err)
            return
        }
        console.log("parcels has been associated to tour " + idTour)
    })

}

const getLastParcels = () => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT id FROM parcel WHERE "idTour" IS NULL LIMIT 6`, (err, res) => {
            if (err) {
                reject(new SqlError("Une erreur SQL est survenue lors de la récupération des tournées", err));
                return
            }

            let parcels = [];
            if (res.rows != null) {
                res.rows.forEach(e => parcels.push(Object.assign(new Parcel(), e)));
            }

            resolve(parcels)
        })
    })

}


export default {getParcelsByTour, deliveryParcelByUuid, getParcelByUuid, isTourCanBeCreated, create, associateParcelsToTour, getLastParcels}
