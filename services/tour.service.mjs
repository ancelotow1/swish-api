import {pool} from "./postgres.service.mjs";
import {SqlError} from "../models/errors/sql-error.mjs";
import {TourDelivery} from "../models/tour-delivery.mjs";
import {query} from "express";
import {Parcel} from "../models/parcel.mjs";


const getCurrentTourByDelivery = (uuidDelivery) => {
    const request = {
        text: `select tor.id, tor.date
               from tour tor
                        join delivery_person dp on tor."idDeliveryPerson" = dp.id and dp.uuid = $1
               where tor.date = cast(now() as date)`,
        values: [uuidDelivery],
    }
    return new Promise((resolve, reject) => {
        pool.query(request, (error, result) => {
            if (error) {
                reject(new SqlError("Une erreur SQL est survenue lors de la recherche des livraisons de la tournée", error));
                return;
            }

            let tour = null;
            if (result.rows != null && result.rows.length === 1) {
                tour = Object.assign(new TourDelivery(), result.rows[0]);
            }
            resolve(tour)
        });
    })
}

/**
 *  Get all tours
 */
const getAllTours = () => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM tour`, (err, res) => {
            if (err) {
                reject(new SqlError("Une erreur SQL est survenue lors de la récupération des tournées", err));
                return;
            }

            let tours = [];
            if (res.rows != null) {
                res.rows.forEach(e => tours.push(Object.assign(new TourDelivery(), e)));
            }
            resolve(tours)
        })
    })
}

const create = (result) => {
    const request = {
        text: `INSERT INTO tour (date, state) VALUES ((now() + '1 day'), 'not started') RETURNING id`,
    }

    pool.query(request, (err, res) => {
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        console.log("tour created :)")
        result(null, res.rows[0].id)
    })
}

const associateTourToDeliverer = (idTour, idDeliverer, result) => {
    const request = {
        text: `UPDATE tour SET "idDeliveryPerson" = $1, date = (now() + '1 day') WHERE id = $2`,
        values: [idDeliverer, idTour]
    }
    pool.query(request, (err, _) =>{
        if (err) {
            result(err)
            return
        }
        result(null)
    })
}

const removeTour = (id, result) => {
    const request = {
        text: `DELETE FROM tour WHERE id = $1`,
        values: [id]
    }

    pool.query(`DELETE FROM tour WHERE id = $1`,id, (err, res) =>{
        if (err) {
            console.log("error: ", err)
            result(err, null)
            return
        }
        result(null,res)
    })
}

const updateTourState = (id, state, result) => {
    const request = {
        text: `UPDATE tour SET state = $1 WHERE id = $2`,
        values: [state, id]
    }
    pool.query(request, (err, _) =>{
        if (err) {
            result(err)
            return
        }
        result(null)
    })
}

const getTourById = (id) => {
    const request = {
        text: `select *
               from tour
               where id = $1`,
        values: [id],
    }

    return new Promise((resolve, reject) => {
        pool.query(request, (err, res) => {
            if (err) {
                reject(new SqlError("Une erreur SQL est survenue lors de la récupération des tournées", err));
                return;
            }

            let tour;
            if (res.rows != null) {
                tour = Object.assign(new TourDelivery(), res.rows[0]);
            }
            resolve(tour)
        })
    })
}

const removeParcelsFromTour = (id, result) => {
    const request = {
        text: `UPDATE parcel SET IdTour = null WHERE idTour = $1`,
        values: [id]
    }
    pool.query(request, (err, _) => {
        if (err) {
            result(err)
            return
        }
        result(null)
    })
}

export default {getCurrentTourByDelivery, getAllTours, create, associateTourToDeliverer, getTourById, updateTourState, removeParcelsFromTour}
