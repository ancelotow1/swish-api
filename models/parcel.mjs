class Parcel {
    uuid
    civility
    lastname
    firstname
    phone
    email
    addressStreet
    town
    zipCode
    country
    isDelivered
    dateDelivered
    urlProofDelivered
    idTour
    tour
    status
}

export {Parcel}
