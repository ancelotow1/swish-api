class DeliveryPerson {
    uuid
    name
    firstname
    email
    birthday
    urlPhoto
    login
    password
    currentLongitude
    currentLatitude
}

export {DeliveryPerson}
