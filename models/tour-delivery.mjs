class TourDelivery {
    id
    date
    parcels = []
    state
    idDeliveryPerson
    deliverer
}

export {TourDelivery}
