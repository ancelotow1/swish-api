import multer from "multer"
import path from "path"
import fs from "fs";

const createUpload = (directory) => {
    const __dirname = path.resolve()
    const storage = multer.diskStorage({
        destination: function (request, file, callback) {
            const path = `./uploads/${directory}`
            fs.mkdirSync(path, { recursive: true })
            callback(null, path);
        },
        filename: function (request, file, callback) {
            const filename = `${Date.now()}${file.originalname}`
            callback(null, filename)
        }
    });
    return multer({ storage: storage, limits: {fileSize: 10000000}});
}

export {createUpload}
